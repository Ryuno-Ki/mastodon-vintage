const now = new Date();
const yearInMs = 365 * 24 * 60 * 60 * 1000;

// Since the timeline is loaded using JavaScript, add a small delay
setTimeout(() => {
  const toots = Array.from(document.querySelectorAll(".item-list article"));
  const oldToots = toots
    .filter((toot) => {
      const time = toot.querySelector('time');
      if (!time) {
        return false;
      }

      const datetime = time.getAttribute('datetime');
      if (!datetime) {
        return false;
      }

      const publishedAt = new Date(datetime);
      const isOld = (now - publishedAt) / yearInMs >= 1;
      console.log('Is Old?', toot, isOld);
      return isOld;
    });

  oldToots
    .forEach((toot) => {
      toot.style.filter = 'sepia(100%)';
    });
}, 1000);
